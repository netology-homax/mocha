const express = require('express');
let apiV1 = express.Router();

const User = require('../../model/User');
const Storage = require('../../storage');

let fields = ['name', 'score'];

function get(body, cb) {
  cb(null, Storage.get());
}

function create(body, cb) {
  let name = body.name;
  let score = body.score;
  let user = new User(name, score);
  Storage.create(user);
  cb(null, user);
}

function update(body, cb) {
  let id = body.id;
  let name = body.name;
  let score = body.score;
  let user = Storage.get(id);
  if (user) {
    user.name = name;
    user.score = score;
  }
  cb(null, user);
}

function remove(body, cb) {
  let id = body.id;
  let deleted = Storage.delete(id);
  cb(null, deleted);
}

module.exports = {
  get,
  create,
  update,
  remove
};
