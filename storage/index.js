const User = require('../model/User');

let counter = 0;

class UserStorage extends Array {
  create(user) {
    if (user instanceof User) {
      user.id = ++counter;
      this.push(user);
    }
  }

  get(id) {
    return id ? this.find(user => user.id === +id) : this;
  }

  delete(id) {
    let deleted;
    if (id) {
      let userId = this.findIndex(user => user.id === +id);
      if (userId !== -1) {
        deleted = this[userId];
        this.splice(userId, 1);
      }
      return deleted;
    }
    deleted = [...this];
    this.splice(0, this.length);
    return deleted;
  }
}

module.exports = new UserStorage();
