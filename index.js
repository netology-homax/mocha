const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const apiRest = require('./api/rest');
const rpcApi = require('./api/rpc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.set('port', process.env.PORT || 3000);

app.post('/rpc', function(req, res) {
    const method = rpcApi[req.body.method];

    if (method) {
      method(req.body, function(error ,result) {
        res.json(result);
      });
      return
    }
    res.status(404);
    res.end('Method not found')
});

app.use('/api', apiRest);

app.use(function(req, res) {
  res.type('text/plain');
  res.status(404);
  res.send('404 Page not found');
});

app.use(function(err, req, res, next) {
  console.log(err.stack);
  res.type('text/plain');
  res.status(500);
  res.end('500 - Internal server error');
});

app.listen(app.get('port'), function() {
  console.log(`Express запущен на http://localhost:${app.get('port')};
нажмите Ctrl+C для завершения.`);
});
