const express = require('express');
let chai = require('chai')
  , should = chai.should();
let request = require('supertest');
const bodyParser = require('body-parser');

let User = require('../../../../../model/User');

describe('Api rest v1 users', function() {

    beforeEach(function() {
        delete require.cache[require.resolve('../../../../../storage')];
        delete require.cache[require.resolve('../../../../../api/rest/v1/users')];

        let apiUsers = require('../../../../../api/rest/v1/users');
        this.storage = require('../../../../../storage');

        const app = express();
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));
        this.app = app.use('/', apiUsers);
    });

    it('should get all users', function(done) {
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);
      this.storage.create(user);

      request(this.app)
        .get('/')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, [{
          id: 1,
          name,
          score
        }],done);

    });

    it('should create user', function(done) {
      let name = 'alex';
      let score = 30;

      request(this.app)
        .post('/')
        .send({name, score})
        .expect('Content-Type', /json/)
        .expect(200, {
          id: 1,
          name,
          score
        }, done);

    });

    it('should change user', function(done) {
      let name = 'alex';
      let newName = 'Alex Bran';
      let score = 30;
      let user = new User(name, score);
      this.storage.create(user);

      request(this.app)
        .put(`/${user.id}`)
        .send({name: newName, score})
        .expect('Content-Type', /json/)
        .expect(200, {
          id: user.id,
          name: newName,
          score
        }, done);

    });

    it('should delete user', function(done) {
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);
      this.storage.create(user);

      request(this.app)
        .delete(`/${user.id}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
            res.body.should.deep.equal({
              id: 1,
              name,
              score
            });
            this.storage.length.should.equal(0);
            done();
        });

    });

    it('should delete all users', function(done) {
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);
      this.storage.create(user);

      request(this.app)
        .delete(`/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
            res.body.should.deep.equal([{
              id: 1,
              name,
              score
            }]);
            this.storage.length.should.equal(0);
            done();
        });

    });

});
