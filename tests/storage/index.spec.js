let chai = require('chai')
  , should = chai.should();

let User = require('../../model/User');

describe('Storage', function() {

    beforeEach(function() {
        delete require.cache[require.resolve('../../storage')];
    });

    it('should save new User', function() {
      let storage = require('../../storage');
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);
      storage.create(user);

      let usersCnt = storage.length;

      usersCnt.should.equal(1);

    });

    it('should get created user', function() {
      let storage = require('../../storage');
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);
      storage.create(user);
      let id = storage[0].id;

      let actualUser = storage.get(id);

      actualUser.name.should.equal(name);
      actualUser.score.should.equal(score);

    });

    it('should delete user', function() {
      let storage = require('../../storage');
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);
      storage.create(user);

      let deletedUser = storage.delete(user.id);
      let usersCnt = storage.length;

      usersCnt.should.equal(0);
      deletedUser.should.equal(user);

    });

});
