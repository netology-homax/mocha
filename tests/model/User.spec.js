let chai = require('chai')
  , should = chai.should();

let User = require('../../model/User');

describe('User model', function() {

    it('should create new User', function() {
      let name = 'alex';
      let score = 30;
      let user = new User(name, score);

      let nameActual = user.name;
      let scoreActual = user.score;

      nameActual.should.equal(name);
      scoreActual.should.equal(score);

    });

});
